<?php
/**
 * Container - хранит в себе анономные функции
 * Создает екземпляры класов после set, setShared
 * Добавляем в контейнер - get
 * По необходимости запускаем и создаем обеъкт на лету
 * - set - каждый раз один и тот же обект
 * - setShared - каждый раз один и тот же обект
 */

class Container {
    private $definitions = [];
    private $shared = [];

    public function set($id, $callback){
        $this->shared[$id] = null;
        $this->definitions[$id] = [
            'callback' => $callback,
            'shared' => false,
        ];
    }

    public function setShared($id, $callback){
        $this->shared[$id] = null;
        $this->definitions[$id] = [
            'callback' => $callback,
            'shared' => true,
        ];
    }

    public function get($id){

        if(isset($this->shared[$id])){
            return $this->shared[$id];
        }

        if(!array_key_exists($id, $this->definitions)){
            throw new \Exception('Undefined component - '. $id);
        }

        $definitions = $this->definitions[$id];
        $component = call_user_func($definitions['callback'], $this);

        if($definitions['shared']){
            $this->shared[$id] = $component;
        }

        return $component;
    }
}

class cart {
    public function addCart($val) {
        return 'cart added - ' . $val . PHP_EOL;
    }
}

$container = new Container();

// каждый раз новый обект
$container->set('cart', function (Container $container){
    return new cart();
});

// каждый раз один и тот же обект
$container->setShared('cart', function (Container $container){
    return new cart();
});

$cart1 = $container->get('cart');
$cart2 = $container->get('cart');

echo $cart1->addCart(777);
echo $cart2->addCart(888);

var_dump($container);

