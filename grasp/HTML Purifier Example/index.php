<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Example 1
 * Добавил пареметры при создании екземпляра класса
 */
$purifier_1 = new \HTMLPurifier([
    'AutoFormat.AutoParagraph' => true,
    'HTML.Allowed' => 'p,ul,li,b,i,a[href],pre,div',
    'AutoFormat.Linkify' => true,
    'HTML.Nofollow' => true,
    'Core.EscapeInvalidTags' => true,
]);

$html = <<<EOL
<div>
    <script>alert(1);</script>
    <p>Danger Code with link http://site.com</p>
</div>
EOL;

echo $purifier_1->purify($html) . PHP_EOL;


/**
 * Example 2
 * Добавил параметры в метод purify() класса
 */

$html = <<<EOL
    <script>alert(1);</script>
    <p>Danger Code with link http://site.com</p>
EOL;

$purifier_2 = new \HTMLPurifier();
echo $purifier_2->purify($html, [
        'AutoFormat.AutoParagraph' => true,
        'HTML.Allowed' => 'p,ul,li,b,i,a[href],pre',
        'AutoFormat.Linkify' => true,
        'HTML.Nofollow' => true,
        'Core.EscapeInvalidTags' => true,
    ]) . PHP_EOL;


/**
 * Example 3
 * Заменяею [#158] на ссылку
 * Расширяю клас HTMLPurifier_Filter
 * Добавляю свой postFilter
 */

class IssueFilter extends HTMLPurifier_Filter
{
    public function postFilter($html, $config, $context)
    {
        return preg_replace_callback('/\[#(\d+)\]/', function ($matches) {
            return '<a href="/issues/' . $matches[1] . '">' . $matches[1] . '</a>';
        }, $html);
    }
}

$purifier_3 = new \HTMLPurifier([
    'Filter.Custom' => [
        new IssueFilter(),
    ]
]);

$html = <<<EOL
    <p>Comment</p>
    <p>See more in [#158] and [#162] issues!!!</p>
    <p>And also [#152]!</p>
EOL;

echo $purifier_3->purify($html) . PHP_EOL;