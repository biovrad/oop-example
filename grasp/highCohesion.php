<?php


/**
 * High Cohesion (Высокая степень зацепления)
 * Все методы (в объекте) должны учавствовать в общем процессе
 * В противном случаи вынести метод в статический хелрер
 */
class Empoyee
{
    private $name;
    private $email;

    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $this->filterEmail($email);
        /**
         * Or like this:
        $this->email = self::filterEmail($email);
        **/
    }

    public function rename($name)
    {
        $this->name = $name;
    }

    public function changeEmail($email)
    {
        $this->email = $this->filterEmail($email);
        /**
         * Or like this:
        $this->email = self::filterEmail($email);
        **/
    }

    private static function filterEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
