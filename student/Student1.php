<?php

class Student1
{
    const ZAOCH = 0;
    const OCH = 1;

    protected $firstName;
    protected $lastName;
    protected $type;

    private function __construct($firstName, $lastName, $type)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->type = $type;
    }

    static public function createOch($firstName, $lastName)
    {
        return new self($firstName, $lastName, self::OCH);
    }

    static public function createZaoch($firstName, $lastName)
    {
        return new self($firstName, $lastName, self::ZAOCH);
    }

    public function getFullName()
    {
        return $this->lastName . ' ' . $this->firstName .' - '. $this->type;
    }

}

$student = Student1::createOch('Vasya', 'Pupkin');
$student2 = Student1::createZaoch('Vasya2', 'Pupkin2');


echo $student->getFullName(). PHP_EOL;
echo $student->getFullName(). PHP_EOL;
echo $student2->getFullName(). PHP_EOL;
