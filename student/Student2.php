<?php

class Student2
{
    const OCHN = 0;
    const ZAOCHN = 1;

    private $name;
    private $type;

    public function __construct($name, $type)
    {
        if (!array_key_exists($type, self::getTypeList())) {
            throw new InvalidArgumentException('Неверный тип');
        }
        $this->name = $name;
        $this->type = $type;
    }

    public static function getTypeList()
    {
        return [
            self::OCHN => 'Очный',
            self::ZAOCHN => 'Заочный',
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }
}


$student  = new Student2('Vasya Ivanov', Student2::OCHN);
$student2 = new Student2('Vasy2 Ivano2', Student2::ZAOCHN);

echo $student->getName() .' - '. $student->getType() . PHP_EOL;
echo $student2->getName() .' - '. $student2->getType() . PHP_EOL;