<?php

class Student
{
    public $lastName;
    public $firstName;
    public $birthDate;

    public function __construct($lastName, $firstName, $birthDate) {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->birthDate = $birthDate;
    }

    public function getFullName() {
        return $this->lastName . ' ' . $this->firstName;
    }

    public function getBirthDate()
    {
        return $this->birthDate;
    }
}

class TXTStudentRepository
{
    private $file;

    public function __construct($file) {
        $this->file = $file;
    }

    public function findAll() {
        $rows = file($this->file);
        $students = [];
        foreach ($rows as $row) {
            $values = array_map('trim', explode(';', $row));
            $students[] = new Student($values[0], $values[1], new \DateTime($values[2]));
        }
        return $students;
    }

    public function findAllByBirthDate($date)
    {
        $rows = file($this->file);
        $students = [];
        foreach ($rows as $row) {
            $values = array_map('trim', explode(';', $row));
            if ($values[2] == $date) {
                $students[] = new Student($values[0], $values[1], $values[2]);
            }
        }
        return $students;
    }

    /** не закончил **/
    public static function saveToTxt($students, $file) {
        $rows = [];
        foreach ($students as $student) {
            $rows[] = implode(';', [
                $student->lastName,
                $student->firstName,
                $student->birthDate,
            ]);
        }
        file_put_contents($file, implode(PHP_EOL, $rows));
    }
}

class XMLStudentRepository
{
    private $file;

    public function __construct($file) {
        $this->file = $file;
    }

    public function findAll() {
        $rows = simplexml_load_file($this->file);
        $students = [];
        foreach ($rows->student as $row) {
            $students[] = new Student($row->lastName, $row->firstName, $row->birthDate);
        }
        return $students;
    }
}

class RepositoryFactory
{
    public static function create($type, $file)
    {
        switch ($type) {
            case 'txt':
                $studentRepository = new TXTStudentRepository($file);
                break;
            case 'xml':
                $studentRepository = new XMLStudentRepository($file);
                break;
            default:
                die('Incorrect type ' . $type);
        }
        return $studentRepository;
    }
}

/** txt **/

/** не закончил **/
//$studentRepository = RepositoryFactory::create('txt', 'list.txt');
//$newStudent = new Student('Ivan','Ivanov','01.01.2020');
//$studentRepository::saveToTxt($newStudent, 'list.txt');
/** не закончил **/

$studentRepository = RepositoryFactory::create('txt', 'list.txt');
$students = $studentRepository->findAll();
foreach ($students as $student) {
    echo $student->getFullName().'-'.$student->getBirthDate()->format('Y-m-d').PHP_EOL;
}

echo PHP_EOL;

$findStudentsBy = $studentRepository->findAllByBirthDate('1981-05-01');
foreach ($findStudentsBy as $studentBy) {
    echo $studentBy->getFullName().PHP_EOL;
}
echo PHP_EOL;

###############################################

/** xml **/
$studentRepositoryXml = RepositoryFactory::create('xml', 'list.xml');
$studentsXml = $studentRepositoryXml->findAll();

foreach ($studentsXml as $studentXml) {
    echo $studentXml->getFullName().'-'.$studentXml->getBirthDate().PHP_EOL;
}


//echo "++++++++++++++++++++++++++++++". PHP_EOL;
//$rows = file(__DIR__ . '/list.txt');
//foreach ($rows as $row) {
//    $values = array_map('trim', explode(';', $row));
//    echo $values[0] . ' ' . $values[1] . ' ' . $values[2] . PHP_EOL;
//}